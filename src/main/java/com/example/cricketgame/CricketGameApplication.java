package com.example.cricketgame;

import com.example.cricketgame.entities.Player;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class CricketGameApplication {
	public static void main(String[] args) {
		SpringApplication.run(CricketGameApplication.class, args);
	}
}
