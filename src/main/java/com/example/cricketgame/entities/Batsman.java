package com.example.cricketgame.entities;

public class Batsman extends Player{
    private double average;
    private double strikeRate;
    private int totalRunsScored;
    private int numOfCenturies;
    private int numOfFifties;

    public Batsman() {
    }

    public Batsman(String name, String contactNumber, String dob, int inningsPlayed, double average, double strikeRate, int totalRunsScored, int numOfCenturies, int numOfFifties) {
        super(name, contactNumber, dob, inningsPlayed);
        this.average = average;
        this.strikeRate = strikeRate;
        this.totalRunsScored = totalRunsScored;
        this.numOfCenturies = numOfCenturies;
        this.numOfFifties = numOfFifties;
    }

    public double getAverage() {
        return average;
    }

    public void setAverage(double average) {
        this.average = average;
    }

    public double getStrikeRate() {
        return strikeRate;
    }

    public void setStrikeRate(double strikeRate) {
        this.strikeRate = strikeRate;
    }

    public int getTotalRunsScored() {
        return totalRunsScored;
    }

    public void setTotalRunsScored(int totalRunsScored) {
        this.totalRunsScored = totalRunsScored;
    }

    public int getNumOfCenturies() {
        return numOfCenturies;
    }

    public void setNumOfCenturies(int numOfCenturies) {
        this.numOfCenturies = numOfCenturies;
    }

    public int getNumOfFifties() {
        return numOfFifties;
    }

    public void setNumOfFifties(int numOfFifties) {
        this.numOfFifties = numOfFifties;
    }
}
