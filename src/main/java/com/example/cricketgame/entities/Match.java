package com.example.cricketgame.entities;

import java.util.Date;

public class Match {
    private String name;
    private Date date;
    private String place;
    private Team teamA;
    private Team teamB;

    public Match() {
    }

    public Match(String name, Date date, String place, Team teamA, Team teamB) {
        this.name = name;
        this.date = date;
        this.place = place;
        this.teamA = teamA;
        this.teamB = teamB;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public Team getTeamA() {
        return teamA;
    }

    public void setTeamA(Team teamA) {
        this.teamA = teamA;
    }

    public Team getTeamB() {
        return teamB;
    }

    public void setTeamB(Team teamB) {
        this.teamB = teamB;
    }
}
