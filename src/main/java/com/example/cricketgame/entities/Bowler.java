package com.example.cricketgame.entities;

public class Bowler extends Player{
    private int totalWicketsTaken;
    private double economy;
    private double average;

    public Bowler() {
    }

    public Bowler(String name, String contactNumber, String dob, int inningsPlayed, int totalWicketsTaken, double economy, double average) {
        super(name, contactNumber, dob, inningsPlayed);
        this.totalWicketsTaken = totalWicketsTaken;
        this.economy = economy;
        this.average = average;
    }

    public int getTotalWicketsTaken() {
        return totalWicketsTaken;
    }

    public void setTotalWicketsTaken(int totalWicketsTaken) {
        this.totalWicketsTaken = totalWicketsTaken;
    }

    public double getEconomy() {
        return economy;
    }

    public void setEconomy(double economy) {
        this.economy = economy;
    }

    public double getAverage() {
        return average;
    }

    public void setAverage(double average) {
        this.average = average;
    }
}
