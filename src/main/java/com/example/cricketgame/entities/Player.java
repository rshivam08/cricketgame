package com.example.cricketgame.entities;

public class Player {
    private String name;
    private String contactNumber;
    private String dob;
    private int inningsPlayed;

    public Player() {
    }

    public Player(String name, String contactNumber, String dob, int inningsPlayed) {
        this.name = name;
        this.contactNumber = contactNumber;
        this.dob = dob;
        this.inningsPlayed = inningsPlayed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public int getInningsPlayed() {
        return inningsPlayed;
    }

    public void setInningsPlayed(int inningsPlayed) {
        this.inningsPlayed = inningsPlayed;
    }
}
