package com.example.cricketgame.entities;

public class Over {
    private int overNumber;
    private int ballNumber;
    private int totalScore;
    private String bowler;

    public Over() {
    }

    public Over(int overNumber, int ballNumber, int totalScore, String bowler) {
        this.overNumber = overNumber;
        this.ballNumber = ballNumber;
        this.totalScore = totalScore;
        this.bowler = bowler;
    }

    public int getOverNumber() {
        return overNumber;
    }

    public void setOverNumber(int overNumber) {
        this.overNumber = overNumber;
    }

    public int getBallNumber() {
        return ballNumber;
    }

    public void setBallNumber(int ballNumber) {
        this.ballNumber = ballNumber;
    }

    public int getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(int totalScore) {
        this.totalScore = totalScore;
    }

    public String getBowler() {
        return bowler;
    }

    public void setBowler(String bowler) {
        this.bowler = bowler;
    }
}
