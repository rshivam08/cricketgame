package com.example.cricketgame.entities;

public class Ball {
    private String bowler;
    private String batsman;
    private boolean wicketTaken;
    private int runsScored;

    public Ball() {
    }

    public Ball(String bowler, String batsman, boolean wicketTaken, int runsScored) {
        this.bowler = bowler;
        this.batsman = batsman;
        this.wicketTaken = wicketTaken;
        this.runsScored = runsScored;
    }

    public String getBowler() {
        return bowler;
    }

    public void setBowler(String bowler) {
        this.bowler = bowler;
    }

    public String getBatsman() {
        return batsman;
    }

    public void setBatsman(String batsman) {
        this.batsman = batsman;
    }

    public boolean isWicketTaken() {
        return wicketTaken;
    }

    public void setWicketTaken(boolean wicketTaken) {
        this.wicketTaken = wicketTaken;
    }

    public int getRunsScored() {
        return runsScored;
    }

    public void setRunsScored(int runsScored) {
        this.runsScored = runsScored;
    }
}
