package com.example.cricketgame.entities;

public class Wicket {
    private String bowler;
    private String batsman;
    private int overNumber;

    public Wicket() {
    }

    public Wicket(String bowler, String batsman, int overNumber) {
        this.bowler = bowler;
        this.batsman = batsman;
        this.overNumber = overNumber;
    }

    public String getBowler() {
        return bowler;
    }

    public void setBowler(String bowler) {
        this.bowler = bowler;
    }

    public String getBatsman() {
        return batsman;
    }

    public void setBatsman(String batsman) {
        this.batsman = batsman;
    }

    public int getOverNumber() {
        return overNumber;
    }

    public void setOverNumber(int overNumber) {
        this.overNumber = overNumber;
    }
}
